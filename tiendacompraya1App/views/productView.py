from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from tiendacompraya1App.serializers.productosSerializer import Product, productosSerializer

class ProductView(views.APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = productosSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        #tokenData = {"username":request.data["username"],
                    #"password":request.data["password"]}
        #tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        #tokenSerializer.is_valid(raise_exception=True)

        #return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)
        #return super().post (request, *args, **kwargs)
        stringResponse = {'detail':'Unauthorized Request'}
        return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)