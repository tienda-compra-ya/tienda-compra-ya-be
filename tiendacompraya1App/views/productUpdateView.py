from django.contrib.messages.views import SuccessMessageMixin
from django import forms
from tiendacompraya1App.models.productos import Product
from django.views.generic.edit import UpdateView
from django.urls import reverse





class ProductActualizar(SuccessMessageMixin, UpdateView): 
    models = Product # Llamamos a la clase 'productos' que se encuentra en nuestro archivo 'models.py' 
    forms = Product # Definimos nuestro formulario con el nombre de la clase o modelo 'Productos' 
    fields = "__all__" # Le decimos a Django que muestre todos los campos de la tabla 'productos' de nuestra Base de Datos 
    success_messages = 'Producto Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Producto 
 
    # Redireccionamos a la página principal luego de actualizar un registro o producto
    def get_success_url(self):               
        return reverse('leer') # Redireccionamos a la vista principal 'leer'